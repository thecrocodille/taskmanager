package ru.thecrocodille.taskmanager.entity;

import java.util.ArrayList;

public class Project {
    private String projectName;
    private int projectID;
    private static int counter = 0;
    private ArrayList<Task> tasks = new ArrayList<>();
            
    public Project(String projectName) {
        this.projectName = projectName;
        counter++;
        this.projectID = counter;
    }

    public String getProjectName() {
        return projectName;
    }

    public int getProjectID() {
        return projectID;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
    
    public ArrayList<Task> getTasks(){
        return tasks;
    }
    
}
