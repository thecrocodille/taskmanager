package ru.thecrocodille.taskmanager.program;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import ru.thecrocodille.taskmanager.entity.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<Project> projects = new ArrayList<>();
        String request;
        boolean isStopped = false;
        int id;
        String name;
        while(!isStopped){
            try{
                request = reader.readLine().trim();
                switch (request){
                    case "project-create": {
                        System.out.println("[PROJECT CREATE]");
                        System.out.print("ENTER NAME: ");
                        name = reader.readLine();
                        projects.add(new Project(name));
                        break;
                    }
                    case "project-list": {
                        System.out.println("[PROJECT LIST]");
                        if(!projects.isEmpty()){
                            for (Project project : projects) {
                                System.out.println(project.getProjectID() + ". " + project.getProjectName());
                            }
                        }else { System.out.println("LIST OF PROJECTS IS EMPTY"); }
                        break;
                    }
                    case "project-remove": {
                        System.out.println("[PROJECT REMOVE]");
                        if(!projects.isEmpty()){
                            Iterator<Project> projectIterator = projects.iterator();
                            System.out.print("ENTER PROJECT ID: ");
                            id = Integer.parseInt(reader.readLine());
                            while (projectIterator.hasNext()) {
                                Project projectNext = projectIterator.next();
                                if(projectNext.getProjectID() == id){
                                    projectIterator.remove();
                                    System.out.println("PROJECT REMOVED");
                                }
                            }
                        }else { System.out.println("LIST OF PROJECTS IS EMPTY"); }
                        break;
                    }
                    case "project-clear": {
                        System.out.println("[PROJECT CLEAR]");
                        projects.clear();
                        System.out.println("DONE");
                        break;
                    }
                    case "task-create": {
                        System.out.println("[TASK CREATE]");
                        if(!projects.isEmpty()){
                            System.out.print("ENTER PROJECT ID: ");
                            id = Integer.parseInt(reader.readLine());
                            for (Project project : projects) {
                                if(project.getProjectID() == id){
                                    System.out.print("ENTER TASK NAME: ");
                                    name = reader.readLine();
                                    project.getTasks().add(new Task(name));
                                }
                            }
                        }else { System.out.println("LIST OF PROJECTS IS EMPTY"); }
                        break;
                    }
                    case "task-list": {
                        System.out.println("[TASK LIST]");
                        if(!projects.isEmpty()){
                            System.out.print("ENTER PROJECT ID: ");
                            id = Integer.parseInt(reader.readLine());
                            for (Project project : projects) {
                                if(project.getProjectID() == id){
                                    if(!project.getTasks().isEmpty()){
                                        for (Task task : project.getTasks()) {
                                            System.out.println("    " + task.getTaskID() + ". " + task.getTaskName());
                                        }
                                    }else { System.out.println("LIST OF TASKS IS EMPTY"); } 
                                }
                            } 
                        }else { System.out.println("LIST OF PROJECTS IS EMPTY"); }
                        break;
                    }
                    case "task-remove": {
                        System.out.println("[TASK REMOVE]");
                        if(!projects.isEmpty()){
                            System.out.print("ENTER PROJECT ID: ");
                            id = Integer.parseInt(reader.readLine());
                            for (Project project : projects) {
                                if(project.getProjectID() == id){
                                    System.out.print("ENTER TASK ID: ");
                                    id = Integer.parseInt(reader.readLine());
                                    Iterator<Task> taskIterator = project.getTasks().iterator();
                                    while(taskIterator.hasNext()){
                                        Task nextTask = taskIterator.next();
                                        if(nextTask.getTaskID() == id){
                                            taskIterator.remove();
                                            System.out.println("TASK REMOVED");
                                        }
                                    }
                                }
                            }
                        }else { System.out.println("LIST OF PROJECTS IS EMPTY"); }
                        break;
                    }
                    case "task-clear": {
                        System.out.println("[TASK CLEAR]");
                        System.out.print("ENTER PROJECT ID: ");
                        id = Integer.parseInt(reader.readLine());
                        for (Project project : projects) {
                            if(project.getProjectID() == id){
                                project.getTasks().clear();
                            }
                        }
                        System.out.println("DONE");
                        break;
                    }
                    case "stop": {
                        System.out.println("[STOPPING]");
                        System.out.println("*** BYE ***");
                        isStopped = true;
                        break;
                    }
                    case "help": {
                        System.out.println("help: Show all commands");
                        System.out.println("project-create: Create new project.");
                        System.out.println("project-list: Show all existing projects.");
                        System.out.println("project-remove: Remove selected project.");
                        System.out.println("project-clear: Clear all projects.");
                        System.out.println("project-create: Create new task in.");
                        System.out.println("project-list: Show all existing task in project.");
                        System.out.println("project-remove: Remove selected task.");
                        System.out.println("project-clear: Clear all tasks in selected project.");
                        System.out.println("stop: Exit the TaskManager.");
                        break;
                    }
                    default: {
                        System.out.println("Type \"help\" to se the list of commands.");
                        break;
                    }
                }
            }
            catch (IOException e){
                e.printStackTrace();
            }        
        }
    }
}
